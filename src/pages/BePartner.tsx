import Partner from 'assets/img/partner.svg';
const BePartner = () => {
  return (
    <div className="container mx-auto p-4 mb-24">
      <h1 className="text-2xl text-center mt-24 mb-2 font-extrabold">Be Partner</h1>
      <p className="text-base font-normal text-center mb-12 opacity-60">What We Do Is Thinking And Acting Global</p>
      <section className="bg-white dark:bg-gray-900 grid md:grid-cols-2 gap-20 ">
        <img src={Partner} />
        <div className=" max-w-screen-md">
          <form action="#" className="space-y-8 text-end">
            <div>
              <input
                type="text"
                id="name"
                className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                placeholder="Your Name"
                required
              />
            </div>
            <div>
              <input
                type="text"
                id="name"
                className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                placeholder="Company Name"
                required
              />
            </div>
            <div>
              <input
                type="email"
                id="email"
                className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                placeholder="name@example.com"
                required
              />
            </div>

            <div className="sm:col-span-2">
              <textarea
                id="message"
                rows={6}
                className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg shadow-sm border border-gray-300 focus:ring-primary-500 focus:border-primary-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                placeholder="Leave a comment..."
              ></textarea>
            </div>
            <button
              type="submit"
              style={{ backgroundColor: '#2F44FF', height: '50px', width: '150px' }}
              className=" text-white font-bold py-2 px-4 rounded"
            >
              Send
            </button>
          </form>
        </div>
      </section>
    </div>
  );
};
export default BePartner;
