import { ReactNode } from 'react';

export interface ErrorBoundaryProps {
  children: ReactNode;
}

export interface ErrorBoundaryState {
  hasError: boolean;
}

export interface CustomDivProps {
  type: 'whatWeDo' | 'workingProcess' | 'service';
  icon: string;
  title: string;
  content: string;
}
