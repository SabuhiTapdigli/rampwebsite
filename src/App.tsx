import ScrollToTop from 'components/ScrollToTop';
import Routes from './routes';

const App = () => {
  return (
    <>
      <ScrollToTop />
      <Routes />
    </>
  );
};

export default App;
