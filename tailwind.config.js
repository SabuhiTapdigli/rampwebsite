module.exports = {
  content: [
    './src/**/*.{js,jsx,ts,tsx}',
    'node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}',
    './node_modules/flowbite/**/*.js',
  ],

  theme: {
    colors: {
      transparent: 'transparent',
      white: '#ffffff',
      blue: '#881337',
      purple: 'rgba(47, 68, 255, 0.1)',
      black: '#000000',
      pink: '#FFBFFC',
      shadowBlue: '#C6D9FF',
    },
    extend: {
      fontFamily: {
        'sf-display': ['SF UI Display', 'Arial', 'sans-serif'],
      },
    },
  },
  plugins: [require('flowbite/plugin')],
};
